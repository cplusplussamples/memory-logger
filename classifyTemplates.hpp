#pragma once

using namespace std;

#define TYPE const int

template <typename C, typename = TYPE>
struct exists_INFO {
  static const bool value = false;
};

template <typename C>
struct exists_INFO<C, decltype(C::DEBUG_INFO)> {
  static const bool value = true;
};

template <typename C, typename = TYPE>
struct exists_ERROR {
  static const bool value = false;
};

template <typename C>
struct exists_ERROR<C, decltype(C::DEBUG_ERROR)> {
  static const bool value = true;
};

template <typename C, typename = TYPE>
struct exists_TRACE {
  static const bool value = false;
};

template <typename C>
struct exists_TRACE<C, decltype(C::DEBUG_TRACE)> {
  static const bool value = true;
};

template <typename C, typename = TYPE>
struct exists_CORRECTNESS {
  static const bool value = false;
};

template <typename C>
struct exists_CORRECTNESS<C, decltype(C::DEBUG_CORRECTNESS)> {
  static const bool value = true;
};

template <typename T>
struct is_string {
  static const bool value = false;
};

template <>
struct is_string<string> {
  static const bool value = true;
};

template <typename T, typename Q, typename ENABLE = void>
struct is_std_string {
  static const bool value = false;
};

template <typename T, typename Q>
struct is_std_string<
    T, Q,
    typename enable_if<
        is_string<typename remove_reference<typename remove_pointer<
            typename remove_cv<T>::type>::type>::type>::value>::type> {
  static const bool value = true;
};

template <typename T, typename Q, typename ENABLE = void>
struct is_primitive_type {
  static const bool value = false;
};

template <typename T, typename Q>
struct is_primitive_type<
    T, Q,
    typename enable_if<
        is_integral<typename remove_reference<typename remove_pointer<
            typename remove_cv<T>::type>::type>::type>::value>::type> {
  // Will come here if T is: int, long, short, char, bool, and long long
  static const bool value = true;
};

template <typename T, typename Q>
struct is_primitive_type<
    T, Q,
    typename enable_if<
        is_string<typename remove_reference<typename remove_pointer<
            typename remove_cv<T>::type>::type>::type>::value>::type> {
  // Will come here if T is: string
  static const bool value = true;
};
