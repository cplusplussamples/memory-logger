#include "boost/preprocessor/seq/for_each_i.hpp"
#include "boost/preprocessor/stringize.hpp"
#include "boost/preprocessor/variadic/size.hpp"
#include "boost/preprocessor/variadic/to_seq.hpp"

#include "changeType.hpp"

#pragma once

/** IMPORTANT: For now it is assumed that the debug option provided by the
 * user is either "INFO", "TRACE", "ERROR", or "CORRECTNESS." Any thing
 * different will not be recognized.
 **/

#define REM(...) __VA_ARGS__
#define EAT(...)

#define PICK_FIRST(first, ...) first

#define SPLIT(...) __VA_ARGS__,

/* Example of call:
 * DECLARE_VARIABLE(name, level_and_type)
 * DECLARE_VARIABLE_IMPL(name, level_and_type, _)
 * level_and_type::type_name name;
 */
#define DECLARE_VARIABLE(name, class) DECLARE_VARIABLE_IMPL(name, class)
#define DECLARE_VARIABLE_IMPL(name, className) className::variable_type name;

#define TYPEOF(class) TYPEOF_IMPL(class)
#define TYPEOF_IMPL(className) className::variable_type

/* Example of call:
 * LEVEL_AND_TYPE((TRACE, LOG, const string) name)
 * EXTRACT_LEVEL_AND_TYPE(2, TRACE, LOG, const string, name)
 * STORE_OPTIONS_2(TRACE, LOG, const string, name)
 */
#define LEVEL_AND_TYPE(...)                                         \
  EXTRACT_LEVEL_AND_TYPE(BOOST_PP_VARIADIC_SIZE(SPLIT __VA_ARGS__), \
                         SPLIT __VA_ARGS__)

/* Note the macro below does not call STORE_OPTIONS directly because a direct
 * call would not force the preprocessor to expand NUM_ARGS (options argument).
 * Without, the expantions of NUM_ARGS, the compiler will not know which
 * STORE_OPTIONS macro to call.
 * By calling a different macro which calls STORE_OPTIONS, the preprocessor will
 * expand NUM_ARGS first and then will call STORE_OPTIONS.
 */
#define EXTRACT_LEVEL_AND_TYPE(options, ...) \
  CALL_STORE_OPTIONS(options, __VA_ARGS__)

#define CALL_STORE_OPTIONS(options, ...) STORE_OPTIONS_##options(__VA_ARGS__)

/**
 * None of the STORE_OPTIONS macro use var_name for anything. However, it is
 * easier to just provide var_name rather than spend the time trying to remove
 * it from the arguments.
 */
#define STORE_OPTIONS_2(var_type, var_name)   \
  /* This is used if no debug level option is \
   * provided. In this case, all options are  \
   * considered as set.                       \
   */                                         \
  static const int DEBUG_INFO = 1;            \
  static const int DEBUG_ERROR = 1;           \
  static const int DEBUG_TRACE = 1;           \
  static const int DEBUG_CORRECTNESS = 1;     \
  using variable_type = var_type;

#define STORE_OPTIONS_3(opt1, var_type, var_name) \
  static const int DEBUG_##opt1 = 1;              \
  using variable_type = var_type;

#define STORE_OPTIONS_4(opt1, opt2, var_type, var_name) \
  static const int DEBUG_##opt1 = 1;                    \
  static const int DEBUG_##opt2 = 1;                    \
  using variable_type = var_type;

#define STORE_OPTIONS_5(opt1, opt2, opt3, var_type, var_name) \
  static const int DEBUG_##opt1 = 1;                          \
  static const int DEBUG_##opt2 = 1;                          \
  static const int DEBUG_##opt3 = 1;                          \
  using variable_type = var_type;

#define STORE_OPTIONS_6(opt1, opt2, opt3, opt4, var_type, var_name) \
  static const int DEBUG_##opt1 = 1;                                \
  static const int DEBUG_##opt2 = 1;                                \
  static const int DEBUG_##opt3 = 1;                                \
  static const int DEBUG_##opt4 = 1;                                \
  using variable_type = var_type;

#define PRINT printObj
#define SERIALIZE serializeObj
#define DESERIALIZE deserializeObj

#define REFLECTABLE(...)                                             \
  static const int fields_n = BOOST_PP_VARIADIC_SIZE(__VA_ARGS__);   \
  template <int N, typename T = int>                                 \
  struct level_and_type {};                                          \
  template <int N, typename T>                                       \
  struct field_data {};                                              \
  void PRINT() { for_each_field_print<fields_n, 0>(*this); }         \
  void SERIALIZE(MemoryLogger& messageBuf) {                         \
    for_each_field_serialize<fields_n, 0>(*this, messageBuf);        \
  }                                                                  \
  void DESERIALIZE(char* messageBuf, int& size, int pos,             \
                   int remainingLength) {                            \
    int reservedLength = remainingLength;                            \
    int reservedPos = pos;                                           \
    for_each_field_deserialize<fields_n, 0>(*this, messageBuf, size, \
                                            remainingLength, pos);   \
    remainingLength = reservedLength - (pos - reservedPos);          \
    size = pos - reservedPos;                                        \
  }                                                                  \
  BOOST_PP_SEQ_FOR_EACH_I(REFLECT_EACH, data,                        \
                          BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))

#define REFLECT_EACH(r, data, i, x)                                            \
  template <typename T>                                                        \
  struct level_and_type<i, T> {                                                \
    LEVEL_AND_TYPE(x)                                                          \
  };                                                                           \
  DECLARE_VARIABLE(EAT x, level_and_type<i>)                                   \
  template <typename T>                                                        \
  struct field_data<i, T> {                                                    \
    field_data(T& self) : self(self) {}                                        \
                                                                               \
    template <typename Q = T,                                                  \
              typename enable_if<is_std_string<TYPEOF(level_and_type<i>),      \
                                               Q>::value>::type* = nullptr>    \
    void deserialize(char* empty, int& size, int& remainingLength, int pos) {  \
      if (remainingLength == 0) {                                              \
        return;                                                                \
      }                                                                        \
      char value[remainingLength];                                             \
      strcpy(value, &empty[pos]);                                              \
      TYPEOF(level_and_type<i>)& pointerTo = (self.EAT x);                     \
      const_cast<typename remove_const<TYPEOF(level_and_type<i>)>::type&>(     \
          pointerTo) = string(value);                                          \
      size = ((self.EAT x).length() + 1);                                      \
      remainingLength -= size;                                                 \
    }                                                                          \
                                                                               \
    template <                                                                 \
        typename Q = T,                                                        \
        typename enable_if<                                                    \
            (!is_std_string<TYPEOF(level_and_type<i>), Q>::value) &&           \
            (is_primitive_type<TYPEOF(level_and_type<i>), Q>::value)>::type* = \
            nullptr>                                                           \
    void deserialize(char* empty, int& size, int& remainingLength, int pos) {  \
      size = sizeof(TYPEOF(level_and_type<i>));                                \
      if (size <= remainingLength) {                                           \
        TYPEOF(level_and_type<i>)& pointerTo = (self.EAT x);                   \
        std::memcpy(                                                           \
            &const_cast<                                                       \
                typename remove_const<TYPEOF(level_and_type<i>)>::type&>(      \
                pointerTo),                                                    \
            &empty[pos], size);                                                \
        remainingLength -= size;                                               \
      }                                                                        \
    }                                                                          \
                                                                               \
    template <typename Q = T,                                                  \
              typename enable_if<!is_primitive_type<                           \
                  TYPEOF(level_and_type<i>), Q>::value>::type* = nullptr>      \
    void deserialize(char* empty, int& size, int& remainingLength, int pos) {  \
      (self.EAT x).DESERIALIZE(empty, size, pos, remainingLength);             \
    }                                                                          \
                                                                               \
    template <                                                                 \
        typename Q = T,                                                        \
        typename enable_if<(is_primitive_type<TYPEOF(level_and_type<i>),       \
                                              Q>::value)>::type* = nullptr>    \
    void serialize(MemoryLogger& memlog) {                                     \
      memlog << get();                                                         \
    }                                                                          \
                                                                               \
    template <typename Q = T,                                                  \
              typename enable_if<!is_primitive_type<                           \
                  TYPEOF(level_and_type<i>), Q>::value>::type* = nullptr>      \
    void serialize(MemoryLogger& memlog) {                                     \
      (self.EAT x).SERIALIZE(memlog);                                          \
    }                                                                          \
                                                                               \
    template <typename Q = T,                                                  \
              typename std::enable_if<is_primitive_type<                       \
                  TYPEOF(level_and_type<i>), Q>::value>::type* = nullptr>      \
    void print() {                                                             \
      cout << name() << " : " << get() << endl;                                \
      return;                                                                  \
    }                                                                          \
                                                                               \
    template <typename Q = T,                                                  \
              typename std::enable_if<!is_primitive_type<                      \
                  TYPEOF(level_and_type<i>), Q>::value>::type* = nullptr>      \
    void print() {                                                             \
      (self.EAT x).PRINT();                                                    \
      return;                                                                  \
    }                                                                          \
                                                                               \
    typename make_const<T, TYPEOF(level_and_type<i>)>::type& get() {           \
      return self.EAT x;                                                       \
    }                                                                          \
                                                                               \
    const string name() { return BOOST_PP_STRINGIZE(EAT x); }                  \
                                                                               \
    T& self;                                                                   \
  };

#define LEVEL INFO

#define TO_STRING(...) TO_STRING_IMPL(__VA_ARGS__)
#define TO_STRING_IMPL(...) BOOST_PP_STRINGIZE(__VA_ARGS__)

#define PRINT_EACH(type, obj, n, level) PRINT_EACH_IMPL(type, obj, n, level)
#define PRINT_EACH_IMPL(type, obj, n, level)                              \
  if (exists_##level<typename type::template level_and_type<n>>::value) { \
    (typename type::template field_data<n, type>(obj)).print();           \
  }

#define SERIALIZE_EACH(type, obj, n, messageBuf, level) \
  SERIALIZE_EACH_IMPL(type, obj, n, messageBuf, level)
#define SERIALIZE_EACH_IMPL(type, obj, n, messageBuf, level)                  \
  if (exists_##level<typename type::template level_and_type<n>>::value) {     \
    (typename type::template field_data<n, type>(obj)).serialize(messageBuf); \
  }

#define DESERIALIZE_EACH(type, obj, n, messageBuf, size, remainingLength, pos, \
                         level)                                                \
  DESERIALIZE_EACH_IMPL(type, obj, n, messageBuf, size, remainingLength, pos,  \
                        level)
#define DESERIALIZE_EACH_IMPL(type, obj, n, messageBuf, size, remainingLength, \
                              pos, level)                                      \
  if (exists_##level<typename type::template level_and_type<n>>::value) {      \
    (typename type::template field_data<n, type>(obj))                         \
        .deserialize(messageBuf, size, remainingLength, pos);                  \
  }

#define DEFINE_DESERIALIZE(type)                                          \
  /**                                                                     \
   * The function's address will be stored inside MemoryLogger which will \
   * invoke this function when deserializing MemoryLogger::messageBuf.    \
   */                                                                     \
  static void deserializeMessage(char* messageBuf, int pos,               \
                                 int remainingLength) {                   \
    int size = 0;                                                         \
    static type p;                                                        \
    for_each_field_deserialize<fields_n, 0>(p, messageBuf, size,          \
                                            remainingLength, pos);        \
    p.PRINT();                                                            \
  }
