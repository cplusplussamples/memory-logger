#include "MemoryLogger.hpp"
#include "macros.hpp"

#pragma once

using namespace std;

template <int N, int n, typename C>
    typename enable_if < n<N>::type for_each_field_print(C& p) {
  PRINT_EACH(C, p, n, LEVEL);
  for_each_field_print<N, n + 1, C>(p);
}

template <int N, int n, typename C>
typename enable_if<n >= N>::type for_each_field_print(C& p) {
  return;
}

template <int N, int n, typename C>
    typename enable_if <
    n<N>::type for_each_field_serialize(C& p, MemoryLogger& messageBuf) {
  SERIALIZE_EACH(C, p, n, messageBuf, LEVEL);
  for_each_field_serialize<N, n + 1, C>(p, messageBuf);
}

template <int N, int n, typename C>
typename enable_if<n >= N>::type for_each_field_serialize(
    C& p, MemoryLogger& messageBuf) {
  return;
}

template <int N, int n, typename C>
    typename enable_if <
    n<N>::type for_each_field_deserialize(C& p, char* messageBuf, int& size,
                                          int& remainingLength, int& pos) {
  DESERIALIZE_EACH(C, p, n, messageBuf, size, remainingLength, pos, LEVEL);
  cout << "Deserializing at position: " << pos << " with size " << size << endl;
  pos += size;
  for_each_field_deserialize<N, n + 1, C>(p, messageBuf, size, remainingLength,
                                          pos);
}

template <int N, int n, typename C>
typename enable_if<n >= N>::type for_each_field_deserialize(
    C& p, char* messageBuf, int& size, int& remainingLength, int& pos) {
  return;
}
