#pragma once

template <typename M, typename N>
struct make_const {
  typedef N type;
};

template <typename M, typename N>
struct make_const<const M, N> {
  typedef typename add_const<N>::type type;
};

