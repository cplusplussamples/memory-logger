#include <functional>
#include <iostream>
#include <sstream>
#include <type_traits>

#include "MemoryLogger.hpp"
#include "classifyTemplates.hpp"
#include "for_field.hpp"
#include "macros.hpp"

using namespace std;

/**
 * The following classes are used for testing only.
 */
struct dummy {
  dummy() : dummyName("dummy"), dummyNo(1) {}

  dummy(string custom, int no) : dummyName(custom), dummyNo(no) {}

  DEFINE_DESERIALIZE(dummy);

  REFLECTABLE((string)dummyName, (INFO, int)dummyNo);
};

struct Person {
  // Person() : name("static"), age(-1), farm("sheep"), num(0) {}
  Person() : name("static"), age(-1), dummy_obj("sheep", 0) {}
  Person(const string name, int age, string dummyName = "custom",
         int dummyNo = 2, string test = "test")
      : name(name), age(age), dummy_obj(dummyName, dummyNo), test(test) {}
  // Person(const string name, int age, const string farm = "sheep",
  //        long long num = 20)
  //     : name(name), age(age), farm(farm), num(num) {}

  DEFINE_DESERIALIZE(Person);

  REFLECTABLE((const string)name, (INFO, int)age, (dummy)dummy_obj,
              (string)test)
  // REFLECTABLE((string)name, (INFO, const int)age, (const string)farm,
  //             (CORRECTNESS, long long)num)
};

struct test_level_type_macro {
  LEVEL_AND_TYPE((TRACE, INFO, int)age)
  DECLARE_VARIABLE(EAT(TRACE, INFO, int) age, test_level_type_macro)
  static const bool info_exists = exists_INFO<test_level_type_macro>::value;
  static const bool error_exists = exists_ERROR<test_level_type_macro>::value;
  static const bool trace_exists = exists_TRACE<test_level_type_macro>::value;
};

int main() {
  // ========================================================== Setup ====
  Person p("Timothy", 45, "cows and veggies", 230, "success");

  cout << "Var args in person class : " << Person::fields_n << endl;

  for_each_field_print<Person::fields_n, 0, Person>(p);

  cout << endl << endl;

  // ==================================================== Test basic =====

  string lengthStr = "45";
  int length = stoi(lengthStr);
  int maxLength = length;
  int remainingLength = length;
  MemoryLogger memlog(lengthStr);

  memlog.testingONLY_setHead(length - 12);

  (typename Person::template field_data<1, Person>(p)).print();
  (typename Person::template field_data<0, Person>(p)).print();
  (typename Person::template field_data<2, Person>(p)).print();

  memlog.startSerializing();
  (typename Person::template field_data<1, Person>(p)).serialize(memlog);
  (typename Person::template field_data<0, Person>(p)).serialize(memlog);
  (typename Person::template field_data<2, Person>(p)).serialize(memlog);
  memlog.endSerializing<Person>();

  Person newP("newP", 0);
  cout << "All messages: " << memlog.printMessage() << endl;
  cout << "-------------------------------------------" << endl;

  (typename Person::template field_data<1, Person>(newP))
      .deserialize(memlog.getMessageBuf(), length, remainingLength, 0 + 16);
  (typename Person::template field_data<0, Person>(newP))
      .deserialize(memlog.getMessageBuf(), length, remainingLength, 4 + 16);
  (typename Person::template field_data<2, Person>(newP))
      .deserialize(memlog.getMessageBuf(), length, remainingLength, 12 + 16);
  (typename Person::template field_data<1, Person>(newP)).print();
  (typename Person::template field_data<0, Person>(newP)).print();
  (typename Person::template field_data<2, Person>(newP)).print();

  cout << endl << endl;

  // ====================================== Test MemoryLogger functions =====
  lengthStr = "112";
  length = stoi(lengthStr);
  maxLength = length;
  remainingLength = length;
  MemoryLogger memlog_for_each(lengthStr);

  memlog_for_each.startSerializing();
  for_each_field_serialize<Person::fields_n - 1, 0, Person>(p, memlog_for_each);
  memlog_for_each.endSerializing<Person>();

  memlog_for_each.startSerializing();
  for_each_field_serialize<Person::fields_n - 1, 0, Person>(p, memlog_for_each);
  memlog_for_each.endSerializing<Person>();

  Person synthesizedPerson("synthesized", 1);
  int pos = 16;
  for_each_field_deserialize<Person::fields_n, 0, Person>(
      synthesizedPerson, memlog_for_each.getMessageBuf(), length,
      remainingLength, pos);

  Person synthesizedPerson1("synthesized1", 2);
  pos = 72;
  int remainingLengthSaved = remainingLength;
  for_each_field_deserialize<Person::fields_n, 0, Person>(
      synthesizedPerson1, memlog_for_each.getMessageBuf(), length,
      remainingLength, pos);

  for_each_field_print<Person::fields_n, 0, Person>(p);
  cout << "-------------------------------------------" << endl;
  for_each_field_print<Person::fields_n, 0, Person>(synthesizedPerson);
  cout << "-------------------------------------------" << endl;
  for_each_field_print<Person::fields_n, 0, Person>(synthesizedPerson1);
  cout << "-------------------------------------------" << endl;
  cout << "Deserialized using static function: " << endl;
  pos = 72;
  Person::deserializeMessage(memlog_for_each.getMessageBuf(), pos,
                             remainingLengthSaved);

  cout << endl << endl;

  // ================================== Test MemoryLogger with overflow =====
  lengthStr = "112";
  length = stoi(lengthStr);
  maxLength = length;
  remainingLength = length;

  memlog_for_each.startSerializing();
  for_each_field_serialize<Person::fields_n, 0, Person>(p, memlog_for_each);
  memlog_for_each.endSerializing<Person>();

  memlog_for_each.startSerializing();
  for_each_field_serialize<Person::fields_n, 0, Person>(p, memlog_for_each);
  memlog_for_each.endSerializing<Person>();

  cout << "Deserialized entire messageBuf (with overflow) using "
          "MemoryLogger:deserializeAllMessages"
       << endl;
  memlog_for_each.deserializeAllMessages();

  cout << endl << endl;

  // ============================================ Test is_primitive_type ====
  cout << "is_primitive_type<const string> : "
       << (is_primitive_type<const string, int>::value == true ? "true"
                                                               : "false")
       << endl;
  cout << "is_primitive_type<int> : "
       << (is_primitive_type<int, int>::value == true ? "true" : "false")
       << endl;

  cout << "is_primitive_type<Person> : "
       << (is_primitive_type<Person, int>::value == true ? "true" : "false")
       << endl;

  cout << endl << endl;

  // ================================================ Test is_str_string ====
  cout << "Person<0> not is_std_string : "
       << (!is_std_string<TYPEOF(typename Person::template level_and_type<0>),
                          int>::value == true
               ? "true"
               : "false")
       << endl;

  cout << "is_std_string<const string> : "
       << (is_std_string<const string, int>::value == true ? "true" : "false")
       << endl;

  cout << "is_std_string<string> : "
       << (is_std_string<string, int>::value == true ? "true" : "false")
       << endl;

  cout << "is_std_string<long long> : "
       << (is_std_string<long long, int>::value == true ? "true" : "false")
       << endl;

  cout << endl << endl;

  // ========================================= Test LEVEL_AND_TYPE macro ====
  cout << "test_level_type_macro has INFO on age: "
       << (test_level_type_macro::info_exists == true ? "true" : "false")
       << endl;
  cout << "test_level_type_macro has ERROR on age: "
       << (test_level_type_macro::error_exists == true ? "true" : "false")
       << endl;
  cout << "test_level_type_macro has TRACE on age: "
       << (test_level_type_macro::trace_exists == true ? "true" : "false")
       << endl;

  cout << endl << endl;

  /*****
  for (int i = 0; i < 5; ++i) {
    ostringstream oss;
    if (i % 3 == 0) {
      oss << "This is message number: " << i;
    } else if (i % 7 == 0) {
      oss << "This is a medium
          sized message number
          : " << i; } else { oss << " This is a short message number : " << i;
    }

    mem_logger1.storeMessage(oss.str());
  }

  auto dumpMessages = [&](const string& message) -> bool {
    cout << message << endl;
    return true;
  };

  mem_logger1.readAllMessages(dumpMessages);
  ****/

  return 0;
}
