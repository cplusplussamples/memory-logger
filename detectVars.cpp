#include <iostream>

using namespace std;

struct hasAge {
  hasAge(int a, const string n) : age(a), name(n){};
  int age;
  const string name;
};

struct noIntAge {
  noIntAge(long a, const string n) : age(a), name(n){};
  long age;
  const string name;
};

struct differentAge {
  differentAge(string a) : age(a){};
  string age;
};

struct constIntAge {
  constIntAge(const int a) : age(a){};
  const int age;
};

struct staticConstIntAge {
  static const int age = 78;
};

struct noAge {
  noAge(const string n) : name(n){};
  const string name;
};

#define VAR age
#define TYPE const int

template <typename C, typename D = TYPE>
struct EXISTS {
  static const bool value = false;
};

template <typename C>
struct EXISTS<C, decltype(C::VAR)> {
  static const bool value = true;
};

template <typename C>
typename enable_if<EXISTS<C>::value, bool>::type invokeIfAge(C& obj) {
  cout << "Object has a age member variable whose type is const int" << endl;
  return true;
}

template <typename C>
typename enable_if<!EXISTS<C>::value, bool>::type invokeIfAge(C& obj) {
  cout << "Object does not have a age member variable or is not of the correct "
          "type"
       << endl;
  return false;
}

int main() {
  hasAge a(14, "Ad");
  bool mIsPresent = EXISTS<hasAge>::value;
  cout << "hasAge " << (mIsPresent == true ? "true" : "false") << endl;
  cout << "HasAge: " << endl;
  invokeIfAge(a);  // returns false (type mismatch)

  cout << "ConstIntAge: " << endl;
  constIntAge ca(143);
  invokeIfAge(ca);  // returns true

  cout << "StaticConstIntAge: " << endl;
  staticConstIntAge sa;
  invokeIfAge(sa);  // returns true

  cout << "NoIntAge: " << endl;
  noIntAge ni(0, "Fa");
  invokeIfAge(ni);  // returns false (type mismatch)

  cout << "differentAge: " << endl;
  differentAge da("200");
  invokeIfAge(da);  // returns false (type mismatch)

  cout << "NoAge: " << endl;
  noAge n("Er");
  invokeIfAge(n);  // returns false (no variable age)
  return 0;
}