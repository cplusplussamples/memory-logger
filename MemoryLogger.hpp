#ifndef _IN_MEMORY_LOGGER_
#define _IN_MEMORY_LOGGER_

#include <assert.h>
#include <errno.h>
#include <cmath>
#include <cstring>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <string>
#include <typeinfo>
#include <vector>

#include "classifyTemplates.hpp"

constexpr size_t MAX_MEMORY_BUFFER_SIZE =
    1024UL * 1024UL * 1024UL * 32;  // 32 GB, that's pretty big already
constexpr size_t MIN_MEMORY_BUFFER_SIZE = 0;  // TODO: fix me 1024;

using namespace std;

class MemoryLogger {
 public:
  MemoryLogger(std::string SIZE) {
    size_t index;
    numBytes = ::abs(stoul(SIZE, &index, 0));

    if (index < SIZE.size() - 1) {
      size_t multiplier = 1;
      switch (SIZE[index]) {
        case 'b':
        case 'B':
          break;
        case 'k':
        case 'K':
          multiplier = 1024;
          break;
        case 'm':
        case 'M':
          multiplier = 1024 * 1024;
          break;
        case 'g':
        case 'G':
          multiplier = 1024 * 1024 * 1024;
          break;
        default:
          break;
      }
      numBytes *= multiplier;
    }
    numBytes =
        numBytes > MAX_MEMORY_BUFFER_SIZE ? MAX_MEMORY_BUFFER_SIZE : numBytes;
    numBytes =
        numBytes < MIN_MEMORY_BUFFER_SIZE ? MIN_MEMORY_BUFFER_SIZE : numBytes;
    messageBuf = new char[numBytes];
    messageCopy = new char[numBytes];
    error = 0;
    if (messageBuf == nullptr) {
      error = ENOMEM;
    }
    head = tail = messageStart = 0UL;
    wrapped = false;
    lastByteOffset = 0;
  }

  uint64_t getNextOffset(uint64_t cOffset) {
    if (cOffset >= lastByteOffset) {
      return 0UL;
    }
    if (wrapped && head <= cOffset && tail >= cOffset) {
      return tail;
    }
    uint64_t size = *(reinterpret_cast<uint64_t *>(&messageBuf[cOffset]));
    if (cOffset + size < lastByteOffset) {
      return cOffset + size;
    }
    return 0UL;
  }

  const std::string printMessage(uint64_t offset) {
    bool debug = true;
    if (offset >= lastByteOffset) {
      return "Offset in no man's land. Last byte : " +
             std::to_string(lastByteOffset) +
             " and offset : " + std::to_string(offset);
    }
    if (wrapped && head <= offset && tail >= offset) {
      return "Offset is between head (" + std::to_string(head) +
             ") and tail (" + std::to_string(tail) +
             "). Offset: " + std::to_string(offset);
    }

    uint64_t size = *(reinterpret_cast<uint64_t *>(&messageBuf[offset]));
    offset += sizeof(uint64_t);
    size -= sizeof(uint64_t);
    std::string message(&messageBuf[offset], size);
    return message;
  }

  void deserializeMessage(uint64_t offset) {
    uint64_t size = *(reinterpret_cast<uint64_t *>(&messageBuf[offset]));
    cout << "Size of object: " << size << endl;
    offset += sizeof(uint64_t);
    uint64_t pointerAddress =
        *(reinterpret_cast<uint64_t *>(&messageBuf[offset]));
    cout << "Address of object method: " << pointerAddress << endl;
    offset += sizeof(uint64_t);
    void (*deserializefunc)(char *, int, int);
    deserializefunc = (void (*)(char *, int, int))pointerAddress;
    deserializefunc(messageBuf, offset, lastByteOffset - offset);
  }

  void deserializeAllMessages() {
    bool debug = true;
    assert(tail <= lastByteOffset);
    uint64_t offset = (tail >= lastByteOffset ? 0UL : tail);
    uint64_t startingOffset = offset;
    if (debug) cout << "Starting at offset: " << std::to_string(offset) << endl;
    do {
      deserializeMessage(offset);
      offset = getNextOffset(offset);
      if (debug) cout << "Offset: " << offset << endl;
    } while (offset != startingOffset);
    if (debug)
      std::cout << "LastByteOffset: " << std::to_string(lastByteOffset)
                << " and tail at " << std::to_string(tail) << " out of "
                << std::to_string(numBytes) << std::endl;
  }

  void readAllMessages(const std::function<bool(const std::string &)> &func) {
    bool debug = true;
    assert(tail <= lastByteOffset);
    uint64_t offset = tail >= lastByteOffset ? 0UL : tail;
    uint64_t startingOffset = offset;
    do {
      if (func(printMessage(offset)) != true) {
        break;
      }
      offset = getNextOffset(offset);
    } while (offset != startingOffset);
    if (debug)
      std::cout << "LastByteOffset: " << std::to_string(lastByteOffset)
                << " and tail at " << std::to_string(tail) << " out of "
                << std::to_string(numBytes) << std::endl;
  }

  char *getMessageBuf() { return messageBuf; }

  void testingONLY_setHead(uint64_t newHead) {
    assert(newHead < numBytes);
    head = messageStart = newHead;
  }

  string printMessage(const char *message = nullptr, int pos = 0) {
    if (message == nullptr) {
      message = messageBuf;
    }
    cout << "Hex " << numBytes << " bytes message" << endl;
    for (int i = pos; i < numBytes; ++i) {
      cout << hex << setfill('0') << setw(2) << (uint)(message[i]) << dec;
    }
    cout << endl;
    return "";
  }

  template <typename T, typename Q = T,
            typename enable_if<is_std_string<T, Q>::value>::type * = nullptr>
  int sizeOfVal(const T &val) {
    return val.length() + 1;  // The plus 1 is for the null-terminated character
  }

  template <
      typename T, typename Q = T,
      typename enable_if<!is_std_string<T, Q>::value &&
                         is_primitive_type<T, Q>::value>::type * = nullptr>
  int sizeOfVal(const T &val) {
    return sizeof(val);
  }

  template <typename T, typename Q = T,
            typename enable_if<is_std_string<T, Q>::value>::type * = nullptr>
  void copyIntoHead(const T &val) {
    cout << "Copying " << val << " at head: " << head << " with length "
         << (val.length() + 1) << " and numBytes " << numBytes << endl;
    strcpy(&messageBuf[head], val.c_str());
    cout << __FUNCTION__ << "::" << __LINE__ << " "
         << "Message in buf : " << printMessage() << endl;
    head += val.length() + 1;
  }

  template <
      typename T, typename Q = T,
      typename enable_if<!is_std_string<T, Q>::value &&
                         is_primitive_type<T, Q>::value>::type * = nullptr>
  void copyIntoHead(const T &val) {
    int sizeVal = sizeOfVal(val);
    cout << "Copying " << val << " at head: " << head << " with size "
         << sizeVal << " and numBytes " << numBytes << endl;
    char vec[sizeVal];
    for (int ind = 0; ind < sizeVal; ++ind) {
      messageBuf[head + ind] = (val >> (ind * 8));
      vec[ind] = (val >> (ind * 8));
    }
    cout << __FUNCTION__ << "::" << __LINE__ << " "
         << "Message in buf : " << printMessage() << endl;
    head += sizeVal;
    cout << "New head: " << head << endl;
  }

  void startSerializing() {
    // We will be storing two ints before the contents of the object. The first
    // will be size of the object and the second will be the typeid of the
    // object.
    int size = sizeof(uint64_t) * 2;
    // Align head
    head = (int)ceil((double)head / 8.0) * 8;
    if (head + size >= numBytes) {
      head = 0UL;
    }
    messageStart = head;
    head += size;

    cout << "Head starting at: " << head << endl;
    cout << "Tail starting at: " << tail << endl;
  }

  template <typename C>
  void endSerializing() {
    assert(messageStart >= 0);
    // Align head
    head = (int)ceil((double)head / 8.0) * 8;
    cout << "Size of message logged: " << (head - messageStart) << endl;
    *(reinterpret_cast<uint64_t *>(&messageBuf[messageStart])) =
        head - messageStart;
    messageStart += sizeof(uint64_t);
    *(reinterpret_cast<uint64_t *>(&messageBuf[messageStart])) =
        reinterpret_cast<uint64_t>(&(C::deserializeMessage));
    messageStart = -1;

    cout << "Head ending at: " << head << endl;
    cout << "Tail ending at: " << tail << endl;
    cout << "Address of function: "
         << reinterpret_cast<uint64_t>(&(C::deserializeMessage)) << endl;
  }

  /**
   * The operator<< method will copy its argument, val, into messageBuf. It is
   * to note that val MUST be a primitive type - i.e. it cannot be a user
   * defined object.
   */
  template <
      typename T, typename Q = T,
      typename enable_if<is_primitive_type<T, Q>::value>::type * = nullptr>
  void operator<<(const T &val) {
    bool debug = true;
    int size = sizeOfVal(val);
    if (not wrapped) {
      if (head + size > numBytes) {
        if (messageStart == 0) {
          // The message is longer than messageBuf. We cannot store it.
          return;
        }
        wrapped = true;
        size += head - messageStart;
        //  Copy all we have written for the current object. We have to move
        //  everything to the start of messageBuf and then, copy val (argument).
        memcpy(&messageCopy[0], &messageBuf[messageStart],
               (numBytes - messageStart));
        auto oldHead = head;
        head = 0UL;
        lastByteOffset = head;
        if (debug)
          std::cout << __FUNCTION__ << "::" << __LINE__ << " "
                    << "First over-flow at head: " << oldHead
                    << "Message coped : " << printMessage(messageCopy) << endl;
        if (debug)
          cout << "Message copy size: " << numBytes << "-" << messageStart
               << " = " << (numBytes - messageStart) << endl;
      } else {
        auto oldHead = head;
        copyIntoHead(val);
        lastByteOffset = head;
        if (debug)
          std::cout << __FUNCTION__ << "::" << __LINE__ << " "
                    << "Message in buf : " << printMessage() << endl;
        return;
      }
    }
    assert(head <= tail);
    uint64_t accumulator = tail - head;
    while (size > accumulator) {
      uint64_t temp = 0;

      if (tail < lastByteOffset) {
        temp = *(reinterpret_cast<uint64_t *>(&messageBuf[tail]));
        tail += temp;
        accumulator += temp;
      } else {
        if (size - accumulator <= numBytes - tail) {
          tail += size - accumulator;
          lastByteOffset = tail;
          accumulator = size;

          if (debug)
            cout << "Tail at " << tail << " and head at " << head << endl;

          if (size > sizeOfVal(val)) {
            assert(head == 0UL);
            int sizeVal = sizeOfVal(val);
            memcpy(&messageBuf[head], &messageCopy[0], size - sizeVal);
            if (debug)
              std::cout << __FUNCTION__ << "::" << __LINE__ << " "
                        << "Message copied at head (0) "
                        << printMessage(nullptr, head) << endl;
            messageStart = head;
            head += size - sizeVal;
            size = sizeVal;
            if (debug) cout << "new head at: " << head << endl;
          }

          copyIntoHead(val);

          if (debug)
            std::cout << __FUNCTION__ << "::" << __LINE__ << " "
                      << "Message at head(" << head << ")"
                      << printMessage(nullptr, head) << endl;

          return;
        } else {
          tail = accumulator = head = 0UL;
          continue;
        }
      }

      if (size > sizeOfVal(val)) {
        assert(head == 0UL);
        int sizeVal = sizeOfVal(val);
        strncpy(&messageBuf[head], messageCopy, size - sizeVal);
        if (debug)
          std::cout << __FUNCTION__ << "::" << __LINE__ << " "
                    << "Message copied at head (0) "
                    << printMessage(nullptr, head) << endl;
        messageStart = head;
        head += size - sizeVal;
        size = sizeVal;
      }

      copyIntoHead(val);

      if (debug)
        std::cout << __FUNCTION__ << "::" << __LINE__ << " "
                  << "Message " << printMessage() << endl;
    }
  }

  void storeMessage(const std::string &message) {
    if (error || message.size() > numBytes) {
      return;
    }

    bool debug = true;

    auto temp = (message.size() + sizeof(uint64_t)) / sizeof(uint64_t);
    auto size = temp * sizeof(uint64_t) < (message.size() + sizeof(uint64_t))
                    ? (temp + 1) * sizeof(uint64_t)
                    : temp * sizeof(uint64_t);
    if (not wrapped) {
      if (head + size >= numBytes) {
        wrapped = true;
        head = 0UL;
        lastByteOffset = head;
      } else {
        strncpy(&messageBuf[head + sizeof(uint64_t)], message.c_str(),
                message.size());
        *(reinterpret_cast<uint64_t *>(&messageBuf[head])) = size;
        auto oldHead = head;
        head += size;
        lastByteOffset = head;
        if (debug)
          std::cout << __FUNCTION__ << "::" << __LINE__ << " "
                    << "Message at head: " << printMessage(oldHead)
                    << std::endl;
        return;
      }
    }

    assert(head <= tail);
    uint64_t accumulator = tail - head;
    while (size > accumulator) {
      uint64_t temp = 0;

      if (tail < lastByteOffset) {
        temp = *(reinterpret_cast<uint64_t *>(&messageBuf[tail]));
        tail += temp;
        accumulator += temp;
      } else {
        if (size - accumulator <= numBytes - tail) {
          tail += size - accumulator;
          lastByteOffset = tail;
          accumulator = size;

          strncpy(&messageBuf[head + sizeof(uint64_t)], message.c_str(),
                  message.size());
          *(reinterpret_cast<uint64_t *>(&messageBuf[head])) = size;
          if (debug)
            std::cout << __FUNCTION__ << "::" << __LINE__ << " "
                      << "Message at head " << printMessage(head) << std::endl;
          head += size;

          return;
        } else {
          tail = accumulator = head = 0UL;
          continue;
        }
      }
    }

    strncpy(&messageBuf[head + sizeof(uint64_t)], message.c_str(),
            message.size());
    *(reinterpret_cast<uint64_t *>(&messageBuf[head])) = size;
    if (debug)
      std::cout << __FUNCTION__ << "::" << __LINE__ << " "
                << "Message at head " << printMessage(head) << std::endl;
    head += size;

    return;
  }

 private:
  uint64_t numBytes;
  char *messageBuf;
  int error;
  uint64_t head;
  uint64_t tail;
  bool wrapped;
  uint64_t lastByteOffset;
  uint64_t messageStart;
  char *messageCopy;
};

#endif  //_IN_MEMORY_LOGGER_
